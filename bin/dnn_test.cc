/*
 * DNN classification test.
 */

#include <iostream>
#include <sys/time.h>

#include "TTH/CommonClassifier/interface/DNNClassifier.h"

const TLorentzVector makeVector(double pt, double eta, double phi, double mass)
{
    TLorentzVector lv;
    lv.SetPtEtaPhiM(pt, eta, phi, mass);
    return lv;
}

int main()
{
    // create dummy input objects
    std::vector<TLorentzVector> jets4
        = { makeVector(242.816604614, -0.107542805374, 1.25506973267, 24.5408706665),
            makeVector(191.423553467, -0.46368226409, 0.750520706177, 30.5682048798),
            makeVector(77.6708831787, -0.709680855274, -2.53739523888, 10.4904966354),
            makeVector(235.892044067, -0.997860729694, -2.10646605492, 27.9887943268) };
    std::vector<double> jetCSVs4 = { 0.8, 0.7, 0.9, 0.1 };

    std::vector<TLorentzVector> jets5(jets4);
    jets5.push_back(makeVector(52.0134391785, -0.617823541164, -1.23360788822, 6.45914268494));
    std::vector<double> jetCSVs5(jetCSVs4);
    jetCSVs5.push_back(0.3);

    std::vector<TLorentzVector> jets6(jets5);
    jets6.push_back(makeVector(35.6511192322, 0.566395223141, -2.51394343376, 8.94268417358));
    std::vector<double> jetCSVs6(jetCSVs5);
    jetCSVs6.push_back(0.99);

    TLorentzVector lepton
        = makeVector(52.8751449585, -0.260020583868, -2.55171084404, 0.139569997787);
    TLorentzVector met = makeVector(92.1731872559, 0., -1.08158898354, 0.);

    // setup the dnn classifier
    DNNClassifier dnn;

    // evaluation
    DNNOutput output4 = dnn.evaluate(jets4, jetCSVs4, lepton, met);
    DNNOutput output5 = dnn.evaluate(jets5, jetCSVs5, lepton, met);
    DNNOutput output6 = dnn.evaluate(jets6, jetCSVs6, lepton, met);

    // some output
    std::vector<DNNOutput> outputs = { output4, output5, output6 };
    for (size_t i = 0; i < outputs.size(); ++i)
    {
        std::cout << "evaluation for model " << (i+4) << "j:" << std::endl;
        std::cout << "0 - ttH:   " << outputs[i].ttH() << std::endl;
        std::cout << "1 - ttbb:  " << outputs[i].ttbb() << std::endl;
        std::cout << "2 - ttb:   " << outputs[i].ttb() << std::endl;
        std::cout << "3 - tt2b:  " << outputs[i].tt2b() << std::endl;
        std::cout << "4 - ttcc:  " << outputs[i].ttcc() << std::endl;
        std::cout << "5 - ttlf:  " << outputs[i].ttlf() << std::endl;
        std::cout << "6 - other: " << outputs[i].other() << std::endl;
        std::cout << "most probable class: " << outputs[i].mostProbableClass() << std::endl;
        std::cout << std::endl;
    }

    // performance test
    std::cout << "performance test:" << std::endl;
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int t0 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    size_t n = 50000;
    for (size_t i = 0; i < n; ++i)
    {
        dnn.evaluate(jets6, jetCSVs6, lepton, met);
    }
    gettimeofday(&tp, NULL);
    long int t1 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    int diff = t1 - t0;
    std::cout << n << " evalutions took " << (diff / 1000.) << " seconds" << std::endl;
    std::cout << "=> " << (((float)diff) / n) << " ms per evaluation" << std::endl;
}
